package main

import "fmt"

type test struct{
	name string
}

func (t test) TestValue() {
	fmt.Printf("%s\n",t.name)
}

func (t *test) TestPointer() {
	fmt.Printf("%s\n",t.name)
}

func main(){
	t := test{"wang"}

	//这里发生了复制,不受后面修改的影响
	m := t.TestValue

	t.name = "Li"
	m1 := (*test).TestPointer
	//Li
	m1(&t)

	//wang
	m()
}