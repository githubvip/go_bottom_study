//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package main

import (
	"fmt"
	"sync"
)
func main() {
	messages := make(chan int)
	var wg sync.WaitGroup
	// you can also add these one at
	// a time if you need to
	wg.Add(3)
	go func() {
		defer wg.Done()
		//time.Sleep(time.Second * 3)
		messages <- 1
	}()
	go func() {
		defer wg.Done()
		//.Sleep(time.Second * 2)
		messages <- 2
	}()
	go func() {
		defer wg.Done()
		//time.Sleep(time.Second * 1)
		messages <- 3
	}()
	go func() {
		for i := range messages {
			fmt.Println(i)
		}
	}()
	wg.Wait()
}
