//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package main
import (
	"fmt"
	"io/ioutil"
	"log"
	httpNew "net/http"
	"sync"
)
func main() {
	urls := []string{
		"http://api.douban.com/v2/book/isbn/9787218087351",
		"http://ip.taobao.com/service/getIpInfo.php?ip=202.101.172.35",
		"https://jsonplaceholder.typicode.com/todos/1",
	}
	jsonResponses := make(chan string)
	var wg sync.WaitGroup
	wg.Add(len(urls))
	for _, url := range urls {
		go func(url string) {
			defer wg.Done()
			res, err := httpNew.Get(url)
			if err != nil {
				log.Fatal(err)
			} else {
				defer res.Body.Close()
				body, err := ioutil.ReadAll(res.Body)
				if err != nil {
					log.Fatal(err)
				} else {
					jsonResponses <- string(body)
				}
			}
		}(url)
	}
	go func() {
		for response := range jsonResponses {
			fmt.Println(response)
		}
	}()
	wg.Wait()
}
