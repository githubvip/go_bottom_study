package main

import (
	"fmt"
	"runtime"
	"time"
	"sync"
)
//golang中有2种方式同步程序，一种使用channel，另一种就是sync.WaitGroup
//定义任务队列
var waitgroup sync.WaitGroup

func task(num int) {
	fmt.Println(num)
	time.Sleep(1*time.Second)
	waitgroup.Done() //任务完成，将任务队列中的任务数量-1，其实.Done就是.Add(-1)
}

func main() {
	//设置最大的可同时使用的CPU核数和实际cpu核数一致
	runtime.GOMAXPROCS(runtime.NumCPU())
	for i := 0; i < 11; i++ {
		waitgroup.Add(1) //每创建一个goroutine，就把任务队列中任务的数量+1
		go task(i)
	}

	waitgroup.Wait() //Wait()这里会发生阻塞，直到队列中所有的任务结束就会解除阻塞
	fmt.Println("finish")
}