//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package main

import (
	"fmt"
	"strings"
)

func main()  {
	str := "$go_bottom_study__"
	line := strings.Count(str,"_")//某子字符在字符串的数量，为""时，代表字符串的个数
	fmt.Println(line)

	lastLine := strings.LastIndex(str,"_")//某个子串在字符串中的位置
	fmt.Println(lastLine)

	t := strings.Index(str,"_")//某个子串在字符串中的第一个出现位置
	fmt.Println(t)

	s1 := strings.Contains(str,"_")//是否包含子串
	fmt.Println(s1)

	s2 := strings.ContainsAny(str,"_")
	fmt.Println(s2)

	s3 := strings.HasPrefix(str,"$")
	fmt.Println(s3)

	arr := make([]string,2)
	arr[0] = "shirdon"
	arr[1] = "liao"

	s4 := strings.Join(arr,";")
	fmt.Println(s4)
}
