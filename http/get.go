package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main()  {
	resp, err := http.Get("http://localhost:8000/auth?username=test&password=test123")
	if err != nil {
		fmt.Println("resp err=", err)
	}
	defer resp.Body.Close()

	fmt.Println(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("body err=", err)
	}
	fmt.Println(string(body))
	fmt.Println(resp.StatusCode)
	fmt.Println(resp.Proto, resp.ProtoMajor, resp.ProtoMinor)
}
